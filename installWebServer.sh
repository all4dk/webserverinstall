#!/bin/bash
clear

DOMAIN="loc"
DOCUMENT_ROOT="/home/web"

showhelp () {
        echo "                                                Web Server AutoInstall Script  "
        echo "-------------------------------------------------------------------------------"
        echo "НАЗВАНИЕ"
        echo ""
        echo "       Web Server AutoInstall Script"
        echo ""
        echo ""
        echo "ДИРЕКТИВЫ"
        echo ""
        echo "         -d - указать желаемую локальную доменую зону."
        echo ""
        echo "                Синтаксис:"
        echo "                     installWebServer.sh -d <domain_name>"
        echo "                Пример:"
        echo "                     installWebServer.sh -d dev"
        echo "                Значение по умолчанию: loc"
        echo ""
        echo ""
        echo "         -r - указать путь к корневой папке веб-сервера"
        echo ""
        echo "                Синтаксис:"
        echo "                     installWebServer.sh -r <document_root>"
        echo "                Пример:"
        echo "                     installWebServer.sh -r /home/username/server"
        echo "                Значение по умолчанию: /home/web"
        echo ""
        echo ""
}

# WELLCOME MESSAGE
show_wellcome () {
        echo "Привет!"
        echo "Этот скрипт должен быть запущен с правами супер пользователя!"
        echo "Если это так, то для начала установки web-сервера ознакомьтесть с информацией и нажми Enter."
        echo ""
        echo "1) Этот скрипт не подходит для установки на продуктивных системах - только лично для Вас, как для разработчика."
        echo "2) Автор скрипта не несёт ответсвеность за любые последствия использоваия этого скрипта. Если не согласны, то нажмите Ctrl+C и удалите скрипт!"
        echo ""
        echo "Для получения помощи по использованию скрипта закройте программу (Ctrl+С) и запустите ее снова с флагом -h"
        echo ""
        echo "Приятной разработки ;)"
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        read
}

# SHOW LAST MESSAGE
show_goodbye () {
        echo "

        ВНИМАНИЕ!"
        echo "Для корректной работы может потребоваться в свойствах сетевого подключения САМОСТОЯТЕЛЬНО указать DNS сервера явным образом, поставив локальный DNS первым. К примеру так:"
        echo "127.0.0.1, 8.8.8.8, 208.67.222.222"
        echo "Для продолжения нажмите Enter"
        read

        echo "Настройка завершена. Для проверки попробуйте посмотреть информацию о своём сервере на http://info.""$DOMAIN""/"
        echo "Чтобы подключиться к PHPMyAdmin зайдите по адресу http://pma.""$DOMAIN""/"
        echo "Создавайте свои проекты в ""$DOCUMENT_ROOT"
        echo ""
        echo "All4DK 2012г. http://all4dk.blogspot.com/"
        echo "Primebit 2012г. https://bitbucket.org/primebit"
        echo "Для завершения нажмите Enter"
        read
}

# INSTALL APACHE, PHP, MYSQL AND PHPMYADMIN
install_lamp () {
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        echo "Установка Apache и PHP"
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        apt-get install apache2 php5 libapache2-mod-php5 php5-curl --yes
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        echo "Установка MySQL сервера. При установке введите пароль 'root' (без кавычек, маленькими буквами)"
        echo "Для продолжения нажмите Enter"
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        read
        apt-get install mysql-server mysql-client php5-mysql --yes
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        echo "Установка PHPMyAdmin."
        echo "При вопросе об используемом сервере отметьте apache2 (используйте пробел и табуляцию или мышь)"
        echo "Соглашаемся на автоматическое конфигурирование базы данных"
        echo "При запросе пароля вводим 'root' (без кавычек, маленькими буквами)"
        echo "Для продолжения нажмите Enter"
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        read
        apt-get install phpmyadmin --yes
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        echo "Настройка Apache"
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        a2enmod rewrite
        a2enmod vhost_alias

        echo "
<VirtualHost *:80>
        ServerName "$DOMAIN"
        ServerAlias *."$DOMAIN"
        VirtualDocumentRoot "$DOCUMENT_ROOT"/%-2/public
        LogLevel warn
        <Directory \""$DOCUMENT_ROOT"\">
                AllowOverride All
                Options -Indexes
                Order allow,deny
                Allow from all
        </Directory>
</VirtualHost>
" >> /etc/apache2/sites-enabled/000-default
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        echo "Настройка Apache завершена. Перезапускаем."
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        service apache2 restart
}

# INSTALL DNSMASQ
install_dnsmasq () {
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        echo "Установка DNSMASQ"
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        apt-get install dnsmasq --yes

        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        echo "Настройка DNSMASQ"
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        echo "address=/""$DOMAIN""/127.0.0.1" >> /etc/dnsmasq.conf

        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        echo "Настройка DNSMASQ завершена. Перезапускаем."
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        service dnsmasq restart
}

create_document_root () {
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        echo "Создание рабочей директории"
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        if [ -a "$DOCUMENT_ROOT" ]
        then
                echo ""
                echo "-------------------------------------------------------------------------------"
                echo ""
                echo "Директория" "$DOCUMENT_ROOT" "уже существует"
                echo ""
                echo "-------------------------------------------------------------------------------"
                echo ""
        else
                mkdir "$DOCUMENT_ROOT"
        fi
        cd "$DOCUMENT_ROOT"
        chmod 777 ./
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        echo "Рабочая директория создана"
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
}

# CONFIGURING PHPMYADMIN
config_pma () {
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        echo "Создание http://pma.""$DOMAIN""/ для доступа к PHPMyAdmin"
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        cd "$DOCUMENT_ROOT"
        mkdir pma
        mkdir pma/public
        echo "Redirect 301 / http://127.0.0.1/phpmyadmin/" > pma/public/.htaccess
}

# MAKE PHPINFO RESOURCE
config_phpinfo () {
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        echo "Создание http://info.""$DOMAIN""/ для просмотра информации о Web-сервере"
        echo ""
        echo "-------------------------------------------------------------------------------"
        echo ""
        cd "$DOCUMENT_ROOT"
        mkdir info
        mkdir info/public
        echo "<?php phpinfo();" > info/public/index.php
}

# READ PARAMS
while getopts ":r:d:h" optname
do
        if [ "$optname" = "d" ]
        then
                DOMAIN="$OPTARG"
        fi
        
        if [ "$optname" = "r" ] && [ "$OPTARG" != "" ]
        then
                DOCUMENT_ROOT="$OPTARG"
                DOCUMENT_ROOT=${DOCUMENT_ROOT%\/} # удаляем последний слеш, если он есть
        fi

        if [ "$optname" = "h" ]
        then
                showhelp;
                exit 0
        fi
done

# CHECK FOR ROOT PRIVELEGIES
if [ "$(whoami)" != 'root' ]; then
    echo "Скрипт должен быть запущен от пользователя root!"
    echo ""
    echo "Введите ./installWebServer.sh -h для получения справки"
    echo ""
    echo ""
    exit 1;
fi

# COMMANDS WATERFALL
show_wellcome;
install_lamp;
install_dnsmasq;
create_document_root;
config_pma;
config_phpinfo;
show_goodbye;

